module.exports = {
  "presets": [
    ["@babel/preset-env", {
      "modules": false
    }],
    "@babel/preset-react",
  ],
  "plugins": [
    ["@babel/plugin-proposal-class-properties", { "loose": true }]
  ],
  env: {
    test: {
      presets: [
        '@babel/preset-env',
        '@babel/preset-react',
      ],
      plugins: [
        '@babel/plugin-proposal-class-properties',
        'transform-es2015-modules-commonjs',
      ],
    },
  }
}